#!/bin/sh

mkdir -p /boot/efi/EFI/boot

cp /usr/share/shim-signed/shimx64.efi /boot/efi/EFI/boot/shimx64.efi
cp /usr/share/shim-signed/mmx64.efi /boot/efi/EFI/boot/mmx64.EFI
