#!/bin/sh

kernel="linux-zen"
mkdir -p /boot/efi/EFI/Linux
sbsign --key /crypto/secureboot/MOK.key --cert /crypto/secureboot/MOK.crt --output /boot/efi/EFI/Linux/archlinux.efi /boot/archlinux.efi
