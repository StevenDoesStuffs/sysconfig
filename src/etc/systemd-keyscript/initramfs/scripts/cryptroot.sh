#!/bin/sh

hour=$(TZ=":America/Chicago" date +%H)
sleep_start=0
sleep_stop=0

if [ "$hour" -ge "$sleep_start" ] && [ "$hour" -lt "$sleep_stop" ]; then
    exit -1
fi

password=$(systemd-ask-password "Enter PIN for GPG smartcard (? tries remaining): ")

usbid="20a0:42b2"

# wait til device is ready
while [ -z "$(lsusb | grep $usbid)" ]; do
    sleep 0.5
done

export GNUPGHOME="/tmp/cryptroot-gnupg"
rm -rf $GNUPGHOME
mkdir -p $GNUPGHOME
chmod 700 $GNUPGHOME

tries() {
    full=$(gpg --card-status | grep "PIN retry counter : ")
    numbers=${full#"PIN retry counter : "}
    first=${numbers% * *}
    echo $first
}

gpg --import "/etc/systemd-keyscript/pubkey.gpg"

mkdir -p "/crypto/disk/"
filename="root_keyfile.bin"

code=-1
first=true
while [ "$code" -ne "0" ]; do
    if [ "$(tries)" -le "0" ]; then
        echo "PIN entered incorrectly too many times, smartcard locked!"
        exit 1
    fi
    if [ "$first" = false ]; then
        password=$(systemd-ask-password "Enter PIN for GPG smartcard ($(tries) tries remaining): ")
    else
        first=false
    fi
    gpg --pinentry-mode loopback --passphrase "$password" --batch -o "/crypto/disk/$filename" -d "/etc/systemd-keyscript/$filename.gpg"
    code="$?"
done
