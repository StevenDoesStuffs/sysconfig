from pathlib import Path
import shutil
import yaml
import glob
import sys

ROOT = '/'

with open("include.yaml", "r") as f:
    try:
        include = yaml.safe_load(f)
    except yaml.YAMLError as exc:
        print(exc, file=sys.stderr)
        sys.exit(-1)

def find_files(files, include, prefix, add=True):
    if prefix == '': prefix = '.'
    elif prefix == '/': prefix = ''

    if isinstance(include, str):
        result = map(Path, glob.glob(f'{prefix}/{include}', recursive=True))
        result = filter(lambda path: \
            path.exists() and not path.is_dir(), result)
        if add: files.update(result)
        else: files.difference_update(result)
        return

    for child in include:
        is_dict = isinstance(child, dict)
        assert is_dict or isinstance(child, str)

        if is_dict:
            assert len(child) == 1
            [(child_prefix, child_dict)] = child.items()
        else:
            child_prefix = child

        invert = child_prefix[0] == '!'
        if invert: child_prefix = child_prefix[1:]

        if is_dict:
            find_files(files, child_dict,
                f'{prefix}/{child_prefix}', invert ^ add)
        else:
            find_files(files, child_prefix,
                prefix, invert ^ add)

def sync_paths(source, source_prefix, target, target_prefix):
    source_rel = set(map(lambda path: path.relative_to(source_prefix), source))
    target_rel = set(map(lambda path: path.relative_to(target_prefix), target))

    remove = map(lambda path: target_prefix / path, target_rel - source_rel)
    for path in remove:
        assert path.exists()
        path.unlink()
        path = path.parent
        while path.is_dir() and not any(path.iterdir()):
            path.rmdir()
            path = path.parent

    for path in source_rel:
        path_source_abs = source_prefix / path
        path_target_abs = target_prefix / path
        assert path_source_abs.exists()
        if path_target_abs.is_dir():
            shutil.rmtree(path_target_abs)
        
        path_target_abs.parent.mkdir(parents=True, exist_ok=True)
        shutil.copy(path_source_abs, path_target_abs, follow_symlinks=False)

def sync(source_prefix, target_prefix):
    source = set()
    target = set()
    find_files(source, include, str(source_prefix))
    find_files(target, include, str(target_prefix))
    sync_paths(source, source_prefix, target, target_prefix)

def push():
    sync(Path('src'), Path(ROOT))

def pull():
    sync(Path(ROOT), Path('src'))

if __name__ == '__main__':
    assert len(sys.argv) == 2
    if sys.argv[1] == 'push': push()
    elif sys.argv[1] == 'pull': pull()
    else:
        print(f'Invalid command {sys.argv[1]}.', file=sys.stderr)
        sys.exit(-1)
